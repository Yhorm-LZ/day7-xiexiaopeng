**O(objective)**:

Learn what is HTTP and RESTful. HTTP is a protocol make client can send request to server and server can send response to client. Learn what is URI and URL,HTTP methods and status code. Learn the concept of resource,representational,state transfer. And learn how to design Restful API.

Learn how to do pair programing with teammates. Through pair programing I can learn some new ways to write a logic, learn some short cut in IDE.

Learn how to build a Spring Boot project and write controller.Then run the server to resolve client's request.
<br><br>**R(Reflective)**:Happy
<br><br>**I(Interpretive)**:RESTful API can standard the uri,makes it look like resource.When we coding in the SpringBoot project, we needs pay attention to design the request url.Following the style of RESTful API.
<br><br>**D(Decisional)**:Pair programing can let me know more shortcut of IDE, know more ways to realize a logic or fuction.Expanding the width of my coding knowledge.