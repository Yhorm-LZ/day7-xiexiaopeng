package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.Repository.EmployeeRepository;
import com.thoughtworks.springbootemployee.model.Employee;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping("/employees")
public class EmployeeController {
    @Resource
    private EmployeeRepository employeeRepository;

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public Employee create(@RequestBody Employee employee){
        return employeeRepository.addEmployee(employee);
    }

    @GetMapping
    public List<Employee> getEmployees(){
        return employeeRepository.getEmployees();
    }

    @GetMapping("/{id}")
    public Employee getEmployeeById(@PathVariable Long id){
        return employeeRepository.getEmployeeById(id);
    }

    @GetMapping(params = "gender")
    public List<Employee> getEmployeesByGender(@PathParam("gender") String gender){
        return employeeRepository.getEmployeesByGender(gender);
    }

    @PutMapping("/{id}")
    public Employee updateEmployeeAgeAndSalary(@PathVariable Long id,@RequestBody Employee employee){
        return employeeRepository.updateEmployeeAgeAndSalary(id,employee);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteEmployeeById(@PathVariable Long id){
        employeeRepository.deleteEmployeeById(id);
    }

    @GetMapping(params = {"page","size"})
    public List<Employee> getEmployeeByPage(@RequestParam("page")Integer page,@RequestParam("size") Integer size){
        return employeeRepository.getEmployeeByPage(page,size);
    }
}
